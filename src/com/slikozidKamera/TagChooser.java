package com.slikozidKamera;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

/**
 * Created with IntelliJ IDEA.
 * User: hrvoje
 * Date: 12/11/12
 * Time: 5:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class TagChooser extends Activity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.tag_chooser);

        Intent i = this.getIntent();

        String filePath = i.getStringExtra("ime");
        Resources res = getResources();

        Bitmap bitmap = BitmapFactory.decodeFile(filePath);
        BitmapDrawable bd = new BitmapDrawable(res, bitmap);



        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.tagLinear);
        linearLayout.setBackgroundDrawable(bd);


        String[] values = new String[] { "Ivan", "je", "blesav", "Android", "iPhone", "WindowsMobile",
                "Blackberry", "WebOS", "Ubuntu", "Windows7", "Max OS X",
                "Linux", "OS/2" };

        ListView listView = (ListView) findViewById(R.id.tagList);

        ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<String>(this, R.layout.list_item, values);

        listView.setAdapter(adapter);

    }

    public void onBackPressed(){
        super.onBackPressed();
        //Intent i = new Intent(this, CameraActivity.class);
        //startActivity(i);
    }
}